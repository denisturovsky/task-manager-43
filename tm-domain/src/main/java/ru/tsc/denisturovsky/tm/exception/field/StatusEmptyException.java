package ru.tsc.denisturovsky.tm.exception.field;

public class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}
