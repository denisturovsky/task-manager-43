package ru.tsc.denisturovsky.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String email = "";

    @NotNull
    @Column(name = "first_name")
    private String firstName = "";

    @NotNull
    @Column(name = "last_name")
    private String lastName = "";

    @Column
    @NotNull
    private Boolean locked = false;

    @Nullable
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(name = "middle_name")
    private String middleName = "";

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

}