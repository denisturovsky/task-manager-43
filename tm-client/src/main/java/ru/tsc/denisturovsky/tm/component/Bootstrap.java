package ru.tsc.denisturovsky.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.denisturovsky.tm.api.endpoint.*;
import ru.tsc.denisturovsky.tm.api.repository.ICommandRepository;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.command.system.ApplicationExitCommand;
import ru.tsc.denisturovsky.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.denisturovsky.tm.exception.system.CommandNotSupportedException;
import ru.tsc.denisturovsky.tm.repository.CommandRepository;
import ru.tsc.denisturovsky.tm.service.CommandService;
import ru.tsc.denisturovsky.tm.service.LoggerService;
import ru.tsc.denisturovsky.tm.service.PropertyService;
import ru.tsc.denisturovsky.tm.service.TokenService;
import ru.tsc.denisturovsky.tm.util.SystemUtil;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.denisturovsky.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpointClient = ISystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpointClient = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(
            @Nullable final String command,
            boolean checkRoles
    ) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) new ApplicationExitCommand().execute();
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

}
