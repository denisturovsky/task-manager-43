package ru.tsc.denisturovsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.ProjectCompleteByIdRequest;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete project by id";

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().completeByIdProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
