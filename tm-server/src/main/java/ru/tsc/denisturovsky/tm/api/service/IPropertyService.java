package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBUser();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
