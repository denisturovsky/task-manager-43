package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.tsc.denisturovsky.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.denisturovsky.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M> {

    @Nullable
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

}
