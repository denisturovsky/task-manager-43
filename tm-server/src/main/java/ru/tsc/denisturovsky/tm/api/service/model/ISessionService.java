package ru.tsc.denisturovsky.tm.api.service.model;

import ru.tsc.denisturovsky.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}

