package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.dto.IDTORepository;
import ru.tsc.denisturovsky.tm.dto.model.AbstractModelDTO;
import ru.tsc.denisturovsky.tm.enumerated.Sort;

import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

}
