package ru.tsc.denisturovsky.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Date;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Date dateBegin,
            @NotNull Date dateEnd
    ) throws Exception;

}
